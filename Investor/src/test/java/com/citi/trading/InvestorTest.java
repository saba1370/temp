package com.citi.trading;


import static org.junit.Assert.assertEquals;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;



public class InvestorTest {

	public static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}
	
	@Test
	public void testportfolio() throws Exception {
		OrderPlacer mark = new MockMarket();
		
		Investor investor = new Investor(10000, mark);
		investor.buy("MRK", 100, 60);
		assertThat(investor.getCash(), closeTo(4000, 0.00001));
		//Map<String, Double> portfolio = new HashMap<>();
		assertThat(investor.getPortfolio(), hasEntry("MRK",100));
		//assertThat(investor.getCash(), closeTo(4000, 0.00001));

	}
	
	@Test
	public void testinvestor() throws Exception {
		OrderPlacer mark = new MockMarket();
		
		Investor investor = new Investor(20000, mark);
		investor.buy("GOOG", 200, 60);
		investor.buy("GOOG", 100, 60);
		assertThat(investor.getCash(), closeTo(2000, 0.00001));
		assertThat(investor.getPortfolio(), hasEntry("GOOG",300));

	}


 
						
}
